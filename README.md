[![pipeline status](https://gitlab.com/wpdesk/wpdesk-sessions/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wpdesk-sessions/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wpdesk-sessions/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wpdesk-sessions/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wpdesk-sessions/v/stable)](https://packagist.org/packages/wpdesk/wpdesk-sessions) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wpdesk-sessions/downloads)](https://packagist.org/packages/wpdesk/wpdesk-sessions) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wpdesk-sessions/v/unstable)](https://packagist.org/packages/wpdesk/wpdesk-sessions) 
[![License](https://poser.pugx.org/wpdesk/wpdesk-sessions/license)](https://packagist.org/packages/wpdesk/wpdesk-sessions)

#Sessions.

Can store session data.

Implemented session adapters:
- Woocommerce

##Usage
```php
$session_factory = new SessionFactory();
$session = $session_factory->get_woocommerce_session_adapter();
$session->set( 'key', 'value' );
$session_value = $session->get( 'key' );
```